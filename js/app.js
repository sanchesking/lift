(function(){
    window.App = {
        Views: {},
        Models: {},
        Collections: {},
        Running: {}
    };
    // template helper
    window.template = function(id) {
        return _.template( $('#' + id).html() );
    };
    window.fnPuzzles = function(handler){

        if(handler.length){
            handler.wookmark({
                // Prepare layout options.
                align: 'left',
                autoResize: true, // This will auto-update the layout when the browser window is resized.
                container: handler.parent(), // Optional, used for some extra CSS styling
                offset: 16, // Optional, the distance between grid items
                outerOffset: 0, // Optional, the distance to the containers border
                itemWidth: 559 // Optional, the width of a grid item
            });
        }
    };

    window.googleMap = function(obj){
        if(obj.length){
            obj.each(function(i){
                var self =  $(this);

                var address = self.attr('data-address');
                var mapId = "map-id-"+i;
                self.attr('id', mapId);
                // Enable the visual refresh
                //google.maps.visualRefresh = true;

                var map;
                // coordinates for placemark

                function initialize() {
                    // map option
                    var mapOptions = {
                        zoom: 17,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };
                    // define google map
                    map = new google.maps.Map(document.getElementById(mapId),
                        mapOptions);
                    // placemark option

                    var geocoder = new google.maps.Geocoder();
                    geocoder.geocode( { 'address':address}, function(results, status) {
                        if (status == google.maps.GeocoderStatus.OK) {
                            google.maps.event.trigger(map, 'resize');
                            map.setCenter(results[0].geometry.location);
                            var marker = new google.maps.Marker({ // маркер
                                position: results[0].geometry.location,
                                map: map
                            });
                            $('#' + mapId).parents('.show-cont').hide();

                        } else {
                            console.log('Geocode was not successful for the following reason: ' + status);
                        }
                    });
                }
                // init map
                //$(document).ready(function(){
                    initialize();
                //});

            });
        }
    }
}());
