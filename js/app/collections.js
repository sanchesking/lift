/*
Lifts Collection
 */
App.Collections.Lifts = Backbone.Collection.extend({
    model: App.Models.Lift,
    initialize: function(){
        this.sortVar = 'error';
    },

    comparator: function( collection ){
        var that = this;
        return( collection.get( that.sortVar ) );
    }
});

/*
Houses Collection
 */
App.Collections.Houses = Backbone.Collection.extend({
    model: App.Models.House
});

/*
Devices Collection
 */
App.Collections.Devices = Backbone.Collection.extend({
    model: App.Models.Device
});