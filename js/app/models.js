/*
Lift Model
 */
App.Models.Lift = Backbone.Model.extend({
    defaults: {
        id: null,
        name: null,
        house: null,
        device: null,
        error: null,
        breaking: [],
        breakCodes: {},
        description: null,
        breaksList: null
    },
    blockBackground: function() {
        var errorCode = this.get('error'),
            className = '';
        if (errorCode == 0) {
            className = 'blink';
        } else if (errorCode == 1) {
            className = 'bg-danger'
        } else if (errorCode == 2) {
            className = 'bg-warning';
        } else if (errorCode == 3) {
            className = 'bg-info';
        }
        return className;
    },
    getAddress: function() {
        var house = App.Running.housesCollection.findWhere({alias:this.get('house')}),
            address = '';
        if (house) {
            address = house.toJSON().address
        }
        return address;
    }
});

/*
House Model
 */

App.Models.House = Backbone.Model.extend({
    defaults: {
        alias: null,
        name: null,
        address: null,
        device: null,
        description: null
    }
});

/*
Device Model
 */

App.Models.Device = Backbone.Model.extend({
    defaults: {
        alias: null,
        name: null,
        address: null,
        description: null
    }
});
