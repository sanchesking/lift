/*
Lift View
 */
App.Views.Lift = Backbone.View.extend({
    tagName: 'li',
    template: template('liftView'),
    events : {
        "click .blink" : "stopBlinking",
        "click .open-popup" : "openPopup",
        "click .open-house-popup" : 'openHouse',
        "click .open-device-popup" : 'openDevice'
    },

    initialize: function() {
        this.render();
        this.model.on('change reset add remove', this.render, this);
    },
    render: function() {
        var data = _.extend(this.model.toJSON(), {
            blockBackground: this.model.blockBackground(),
            address: this.model.getAddress()
        });
        this.$el.html( this.template( data ) );

        return this;
    },
    stopBlinking: function(e) {
        e.preventDefault();
        this.model.set('error', 1);
    },
    openPopup: function(e) {
        e.preventDefault();
        var popupLift = new App.Views.PopupLift({model: this.model});
        $('#popup-wrapper').html(popupLift.el);
    },
    openHouse: function(e) {
        e.preventDefault();
        var houseModel = App.Running.housesCollection.findWhere({alias: this.model.get('house')});
        var popupHouse = new App.Views.PopupHouse({model: houseModel});
        $('#popup-wrapper').html(popupHouse.el);
        googleMap($(".map"));
    },
    openDevice: function(e) {
        e.preventDefault();
        var deviceModel = App.Running.devicesCollection.findWhere({alias: this.model.get('device')});
        var popupDevice = new App.Views.PopupDevice({model: deviceModel});
        $('#popup-wrapper').html(popupDevice.el);
        googleMap($(".map"));
    }
});

/*
Lifts View
 */
App.Views.Lifts = Backbone.View.extend({
    tagName: 'ul',
    className: 'list-unstyled',

    initialize: function() {
        this.collection.on('change reset add remove', this.render, this);
    },

    render: function() {
        this.$el.empty();
        this.collection.sort( this.collection.comparator ).each(function(lift){
            var liftView = new App.Views.Lift({model: lift});

            this.$el.append(liftView.render().el)
        }, this);
        fnPuzzles($('#app-wrapper > ul > li'));
        return this;
    }
});

/*
Popup Lift View
 */
App.Views.PopupLift = Backbone.View.extend({
    tagName: 'div',
    template: template('popupLiftView'),
    events : {
        "click .off-lift" : "offLift",
        "click .on-lift" : "onLift"
    },
    initialize: function() {
        this.render();
        this.model.on('change reset add remove', this.render, this);
    },
    render: function() {
        var data = _.extend(this.model.toJSON(), {
            address: this.model.getAddress()
        });
        this.$el.html( this.template( data ) );
        return this;
    },
    offLift :function(e) {
        e.preventDefault();
    },
    onLift: function(e) {
        e.preventDefault();
    }
});

/*
House view
 */
App.Views.House = Backbone.View.extend({
    tagName: 'li',
        template: template('houseView'),
    events : {
        "click .open-popup" : "openPopup"
    },

    initialize: function() {
        this.render();
        this.model.on('change reset add remove', this.render, this);
    },
    render: function() {
        this.$el.html( this.template( this.model.toJSON() ) );

        return this;
    },
    openPopup: function(e) {
        e.preventDefault();
/*        var popupLift = new App.Views.PopupHouse({model: this.model});
        $('#popup-wrapper').html(popupLift.el);*/
    }
});

/*
 Houses View
 */
App.Views.Houses = Backbone.View.extend({
    tagName: 'ul',
    className: 'list-unstyled',

    initialize: function() {
        this.collection.on('change reset add remove', this.render, this);
    },

    render: function() {
        this.$el.empty();
        this.collection.each(function(house){
            var houseView = new App.Views.House({model: house});

            this.$el.append(houseView.render().el)
        }, this);

        return this;
    }
});

/*
 Popup House View
 */
App.Views.PopupHouse = Backbone.View.extend({
    tagName: 'div',
    template: template('popupHouseView'),
    initialize: function() {
        this.render();
        this.model.on('change reset add remove', this.render, this);
    },
    render: function() {
        this.$el.html( this.template( this.model.toJSON() ) );

        return this;
    }
});


/*
 Device view
 */
App.Views.Device = Backbone.View.extend({
    tagName: 'li',
    template: template('deviceView'),
    events : {
        "click .open-popup" : "openPopup"
    },

    initialize: function() {
        this.render();
        this.model.on('change reset add remove', this.render, this);
    },
    render: function() {
        this.$el.html( this.template( this.model.toJSON() ) );

        return this;
    },
    openPopup: function(e) {
        e.preventDefault();
        /*        var popupLift = new App.Views.PopupHouse({model: this.model});
         $('#popup-wrapper').html(popupLift.el);*/
    }
});

/*
 Devices View
 */
App.Views.Devices = Backbone.View.extend({
    tagName: 'ul',
    className: 'list-unstyled',

    initialize: function() {
        this.collection.on('change reset add remove', this.render, this);
    },

    render: function() {
        this.$el.empty();
        this.collection.each(function(device){
            var deviceView = new App.Views.Device({model: device});

            this.$el.append(deviceView.render().el)
        }, this);

        return this;
    }
});

/*
 Popup Device View
 */
App.Views.PopupDevice = Backbone.View.extend({
    tagName: 'div',
    template: template('popupDeviceView'),
    initialize: function() {
        this.render();
        this.model.on('change reset add remove', this.render, this);
    },
    render: function() {
        this.$el.html( this.template( this.model.toJSON() ) );
        return this;
    }
});