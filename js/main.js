
(function(){
    $.getJSON( "data/data.json", function( data ) {
        App.Running.liftsCollection = new App.Collections.Lifts(data.lifts);
        App.Running.housesCollection = new App.Collections.Lifts(data.houses);
        App.Running.devicesCollection = new App.Collections.Lifts(data.devices);

        var liftsView = new App.Views.Lifts({collection: App.Running.liftsCollection});

        $('#app-wrapper').html(liftsView.render().el);
        $('.popup').fancybox();
        fnPuzzles($('#app-wrapper > ul > li'));
    });

}());

